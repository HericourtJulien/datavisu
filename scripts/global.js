var cardList = (function () {

    var Card = function () {
        this.el = null;
        this.rank = null;
        this.title = null;
        this.year = null;
        this.real = null;
        this.country = null;
        this.entries = null;
        this.maxRank = 101;

        this.data = {};
    };

    Card.prototype.init = function (el, data) {
        this.el = el;

        this.title = this.el.querySelector("h2");
        this.year = this.el.querySelector("h4");
        this.entries = this.el.querySelector(".card-entries-number");
        this.country = this.el.querySelector(".card-country-name");
        this.real = this.el.querySelector(".card-real-name");

        this.fill(data);
    };

    Card.prototype.build = function (data) {
        this.el = document.createElement("li");
        this.el.classList.add("card");
        this.el.innerHTML = document.querySelector("#card-tmpl").innerHTML;

        this.init(this.el, data);
    };

    Card.prototype.fill = function (data) {
        this.data = data;
        this.el.setAttribute("data-rank", this.data.rank);

        var z = this.maxRank - this.data.rank;

        this.el.setAttribute("style", "z-index: " + z + "; background: url(./img/bg-card-" + this.data.rank + ".jpg) no-repeat center red");
        this.title.textContent = data.title;
        this.year.textContent = data.year;
        this.entries.textContent = data.entries.toLocaleString();
        this.real.textContent = data.real;
        this.country.textContent = data.country;
    };

    var CardList = {

        el: null,
        tl: new TimelineMax(),
        nav: null,
        idCurrent: null,
        current: null,
        currents: null,
        currentOld: null,
        currentsOld: null,
        maxCard: null,
        card: null,
        legendYear: null,


        init: function () {
            this.el = document.getElementById("cards");
            this.nav = document.querySelector("#nav");
            this.load();
        },

        load: function () {
            this.req = new XMLHttpRequest();
            this.req.open("GET", "./scripts/data.json", true);
            this.req.addEventListener("readystatechange", this.loaded.bind(this));
            this.req.send();
        },

        loaded: function () {
            if (this.req.readyState == 4) {
                if (this.req.status == 200) {
                    var datas = JSON.parse(this.req.responseText);
                    var i;
                    for (i = 0; i < datas.length; i++) {
                        this.add({detail: {data: datas[i]}});
                    }
                    this.launchInteractivity();
                }
            }
        },

        launchInteractivity: function () {
            TweenMax.to(document, 2, {onComplete: this.drag, onCompleteScope: this});
            TweenMax.set(document.querySelectorAll('.card-bar'), {autoAlpha: 0, display: "none"});
            TweenMax.set(document.querySelectorAll('.card-title'), {autoAlpha: 0, display: "none"});
            TweenMax.set(document.querySelectorAll('.card-year'), {autoAlpha: 0, display: "none"});

            document.querySelector('.card-prev').addEventListener("click", this.prev.bind(this));
            document.querySelector('.card-next').addEventListener("click", this.next.bind(this));
        },

        add: function (e) {
            var data = e.detail.data;

            var item = new Card();
            item.build(data);

            TweenMax.to(document, 3, {onComplete: this.listenNav, onCompleteParams: [data], onCompleteScope: this});

            this.listen(item);
            this.el.appendChild(item.el);
        },

        listen: function (item) {
            item.el.querySelector(".bg").addEventListener("mouseover", this.mouseOver.bind(this, item.el));
            item.el.querySelector(".bg").addEventListener("click", this.cardClick.bind(this, item.el));
        },

        listenNav: function (data) {
            var nav = new Nav();
            nav.build(data);
            nav.el.querySelector(".bg").addEventListener("mouseover", this.navOver.bind(this, nav.el));
            nav.el.querySelector(".bg").addEventListener("click", this.navClick.bind(this, nav.el));
            this.nav.appendChild(nav.el);
        },

        cardClick: function (el) {
            var cardExtra = el.querySelector(".card-extra");
            var cardExtraClose = el.querySelector(".card-extra-close");

            var tl = new TimelineMax({
                onStart: function () {
                    TweenMax.set(cardExtra, {display: "block"});
                }
            });
            tl.to(cardExtra, .4, {x: 0});
            cardExtraClose.addEventListener("click", this.closeClick.bind(this, cardExtra, tl));
        },

        closeClick: function (el) {
            TweenMax.to(el, .3, {x: el.offsetWidth});
        },

        navClick: function (el) {
            var navRank = el.getAttribute("data-rank");
            var currentRank = document.querySelector(".current").getAttribute("data-rank");
            var wantedCard = navRank - currentRank;

            var i = 0;
            if (wantedCard > 0) {
                for (i; i < wantedCard; i++) {
                    TweenMax.to(document, i / 6, {onComplete: this.next, onCompleteScope: this});
                }
            } else if (wantedCard < 0) {
                for (i; i > wantedCard; i--) {
                    TweenMax.to(document, Math.abs(i) / 6, {onComplete: this.prev, onCompleteScope: this});
                }
            }
        },

        timelineNav: function (el) {
            var barContent = el.querySelector('.nav-bar-content');
            var barRank = el.querySelector('.nav-bar-rank-content');
            var barTitle = el.querySelector('.nav-bar-title-content');
            var barSep = el.querySelector('.nav-bar-sep-content');

            var tlNav = new TimelineMax({
                onStart: function () {
                    TweenMax.set(barContent, {display: "none"});
                    TweenMax.set(barContent, {display: "block"});
                }
            });
            tlNav.timeScale(1);
            tlNav.set(barContent, {display: "none"});
            tlNav.set(barContent, {display: "block"});
            tlNav.to(barSep, .3, {y: 0});
            tlNav.to(barTitle, .6, {y: 0}, "-=.1");
            tlNav.to(barRank, .6, {y: 0}, "-=.6");
            tlNav.pause();

            return tlNav;
        },

        timeline: function (el) {
            var cardBar = el.querySelector('.card-bar');
            var cardTitle = el.querySelector('.card-title');
            var cardYear = el.querySelector('.card-year');

            var tl = new TimelineMax({
                onStart: function () {
                    TweenMax.set(cardBar, {autoAlpha: 1, display: "block"});
                    TweenMax.set(cardTitle, {autoAlpha: 1, display: "block"});
                    TweenMax.set(cardYear, {autoAlpha: 1, display: "block"});
                }
            });

            tl.timeScale(1);
            tl.to(cardBar, .6, {width: 320});
            tl.to(cardTitle, .6, {x: 0}, "-=.4");
            tl.to(cardYear, .6, {x: 0}, "-=.4");
            tl.pause();

            return tl;
        },

        navOver: function (el) {
            var tlNav = this.timelineNav(el);
            this.legendYear = document.querySelector(".legend-year");
            this.legendYear.querySelector(".legend-year-content").innerText = el.getAttribute("data-year");
            var bottom = parseInt(el.style.height) - 5 + "px";
            TweenMax.to(this.legendYear, .4, {bottom: bottom});
            TweenMax.to(this.legendYear, 1, {opacity: 1});
            el.querySelector(".bg").addEventListener("mouseleave", this.navLeave.bind(this, tlNav, el));
            tlNav.play();
        },

        navLeave: function (tlNav) {
            TweenMax.to(this.legendYear, 1, {opacity: 0});
            tlNav.timeScale(3);
            tlNav.reverse();
        },

        mouseOver: function (el) {
            var bgStyle = document.querySelector(".bg-style");
            bgStyle.style.backgroundImage = el.style.backgroundImage;
            TweenMax.to(bgStyle, .5, {opacity: .1});

            var tl = this.timeline(el);
            el.querySelector(".bg").addEventListener("mouseleave", this.mouseLeave.bind(this, tl));
            (elIndex(el) == 2 || elIndex(el) == 102) ? tl.play() : '';
        },

        mouseLeave: function (tl) {
            tl.timeScale(3);
            tl.reverse();

            var bgStyle = document.querySelector(".bg-style");
            TweenMax.to(bgStyle, .5, {opacity: 0});
        },

        drag: function () {
            if (document.innerWidth > 400) {
                var config = {
                    minThrowOutDistance: 600,
                    maxThrowOutDistance: 650,
                    maxRotation: 20,
                    throwOutConfidence: function (offset, element) {
                        return Math.min(Math.abs(offset) / element.offsetWidth, 1);
                    }
                };
            } else {
                var config = {
                    minThrowOutDistance: 10,
                    maxThrowOutDistance: 20,
                    maxRotation: 20,
                    throwOutConfidence: function (offset, element) {
                        return Math.min(Math.abs(offset) / element.offsetWidth, 1);
                    }
                };
            }


            stack = gajus.Swing.Stack(config);

            var i = 0;

            [].forEach.call(document.querySelectorAll('#cards .card'), function (targetElement) {
                i--;
                stack.createCard(targetElement);
                targetElement.classList.add('in-deck');
                stack.getCard(targetElement).throwIn(i * -1000, Math.random() * 20 - 10);
            }.bind(this));

            this.maxCard = document.querySelectorAll('.card').length;
            this.getCurrent();

            stack.on('dragstart', function (e) {
                e.target.classList.remove("no-translate");
            });

            stack.on('dragmove', function (e) {
                this.throwOutConfidence = e.throwOutConfidence;
                var op = 1 - this.throwOutConfidence;
                e.target.style.opacity = op;
            });

            stack.on('dragend', function (e) {
                if (e.target.classList.contains('in-deck')) {
                    e.target.style.opacity = 1;
                    e.target.classList.remove('hide');
                } else {
                    e.target.classList.add('hide');
                }
            });

            stack.on('throwout', function (e) {
                e.target.classList.remove('in-deck');
                this.getCurrent();
            }.bind(this));

            stack.on('throwin', function (e) {
                e.target.classList.add('in-deck');
                this.getCurrent();
            }.bind(this));
        },

        getCurrent: function () {
            if (this.current != null) {
                this.currentsOld = document.querySelectorAll('.current');
                this.currentOld = this.currentsOld[0];
                this.currentOld.classList.remove('current');
                if (this.currentsOld.length > 1) {
                    this.currentsOld[1].classList.remove('current');
                    this.currentsOld[1].querySelector('.nav-bar-content').style.display = "none";
                }
                ;
            }
            var inDeck = document.querySelectorAll('.in-deck').length;
            this.idCurrent = this.maxCard - inDeck;
            this.currents = document.querySelectorAll('[data-rank="' + this.idCurrent + '"]');
            this.current = this.currents[0];
            this.current.classList.add('current');
            if (this.currents.length > 1) {
                this.currents[1].classList.add('current');
                this.navOver(this.currents[1]);
            }
            document.querySelector("#current .current-number").innerHTML = formatNumber(this.idCurrent);
        },

        prev: function () {
            if (this.idCurrent != 0) {
                var prev = document.querySelector('[data-rank="' + (this.idCurrent - 1) + '"]');
                this.card = stack.createCard(prev);
                prev.classList.remove('hide');
                prev.style.opacity = 1;
                this.card.throwIn(Math.random() * 1200 - 600, Math.random() * 20 - 10);
            }
        },

        next: function () {
            if (this.idCurrent != this.maxCard) {
                this.card = stack.createCard(this.current);
                this.card.throwOut(Math.random() * 1200 - 600, Math.random() * 20 - 10);
                this.currentOld.classList.add('hide');
            }
        }
    };

    var Nav = function () {
        this.el = null;
        this.rank = null;
        this.title = null;
        this.bg = null;
        this.maxYear = 2015;
        this.minYear = 1945;
        this.legend = null;

        this.data = {};
    };

    Nav.prototype.init = function (el, data) {
        this.el = el;

        this.title = this.el.querySelector(".nav-bar-title-content");
        this.rank = this.el.querySelector(".nav-bar-rank-content");
        this.bg = this.el.querySelector(".bg");
        this.legend = document.querySelector(".legend");

        this.fill(data);
    };

    Nav.prototype.build = function (data) {
        this.el = document.createElement("div");
        this.el.classList.add("nav-bar");
        this.el.innerHTML = document.querySelector("#nav-bar-tmpl").innerHTML;

        this.init(this.el, data);
    };

    Nav.prototype.fill = function (data) {
        this.data = data;
        this.el.setAttribute("data-rank", this.data.rank);
        this.el.setAttribute("data-year", this.data.year);

        this.title.textContent = data.title;
        this.rank.textContent = "-" + formatNumber(data.rank) + "-";
        var height = parseInt((data.year - this.minYear) / (this.maxYear - this.minYear) * 25) + 10;

        var tl = new TimelineMax();

        tl.to(this.el, 1.5, {height: height, ease: Bounce.easeOut});
        tl.to(this.bg, 1.5, {height: height, ease: Bounce.easeOut});
        tl.to(this.legend.querySelectorAll(".year-number"), .3, {opacity: 1, x: 0});
        tl.to(this.legend.querySelectorAll(".year-bar"), 1, {width: this.legend.offsetWidth + 60}, "-=.4");
    };

    return CardList;
})();

var app = (function (cardList) {
    var App = {
        cardList: null,

        init: function () {
            this.cardList = cardList;
            this.animation();
            console.log("Initialisation");
            document.addEventListener("DOMContentLoaded", this.loaded.bind(this));
        },

        loaded: function () {
            console.log("DOM chargé");
            this.cardList.init();
        },

        animation: function () {
            var pageHeading = document.querySelector(".page-heading");
            var topNav = document.querySelectorAll(".top-navigation");
            var current = document.querySelector("#current");
            var filters = document.querySelector("#filters");
            var copy = document.querySelector("#copyright");

            var tl = new TimelineMax({
                onComplete: function () {
                    var cards = document.querySelectorAll(".card");
                    var i = 0;
                    for (i; i < cards.length; i++)
                        cards[i].classList.add('no-translate');
                }
            });
            tl.to(pageHeading, 1, {opacity: 1}, "+=1");
            tl.to(topNav, 1, {opacity: 1}, "-=1");
            tl.to(current, 1, {opacity: 1}, "-=1");
            tl.to(filters, 1, {opacity: 1}, "-=1");
            tl.to(copy, 1, {right: 14}, "+=1");

            filters.addEventListener('click', this.filters.bind(this));
        },

        filters: function () {
            var filterContent = document.querySelector(".filters-content p");
            var tl = new TimelineMax();
            tl.to(filterContent, .3, {x: 0});
            tl.to(filterContent, .3, {x: -filterContent.offsetWidth - 1}, "+=2");
        }
    };

    return App;
})(cardList);

app.init();


/***************************
 * Functions
 **************************/
var elIndex = function (el) {
    var sib = el.parentNode.childNodes;
    for (var i = 0; i < sib.length; i++) {
        if (el == sib[i]) {
            return i;
        }
    }
};

function formatNumber(value) {
    var zeroes = new Array(4).join("0");
    return (zeroes + value).slice(-3);
}